export interface IConnectData {
  login: string;
  appkey: string;
  token: string;
  sign: string;
}

export interface IConstructorParams {
  appkey?: string;
  secret?: string;
  ssl?: boolean;
  host?: string;
  userAgent?: string;
  errorTelemetry?: boolean;
}

export interface ILoginData {
  login: string;
  accountkey?: string;
  password?: string;
}

export interface IRequestParams {
  api?: any[];
  post?: {
    [key: string]: any;
  };
  named?: {
    [key: string]: any;
  };
}

export interface IUserData {
  isLoggedIn?: boolean;
  login: string | null;
  accountkey?: string | null;
  password?: string | null;
  userkey?: string | null;
}
