import Wykop from '../index';

const wykop = new Wykop({});

it('Makes GET requests with named and API params, resolves promise', () => wykop.request('profiles/entries', {
  api: ['m__b'],
  named: {
    page: '12',
  },
}).then((res: any) => {
  expect(res.error).toBeUndefined();
  expect(typeof res.data).toBe('object');
}));

const INVALID_PASSWORD_ERROR_CODE = 14;
it('Makes POST requests, rejects promise on error', () => wykop.request('login/index', {
  post: {
    login: 'm__b',
    password: 'password',
  },
}).catch((err: any) => {
  expect(err.code).toBe(INVALID_PASSWORD_ERROR_CODE);
}));
