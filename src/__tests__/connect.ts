import fetch from 'node-fetch';
import Wykop from '../index';

const wykop = new Wykop({});

const HTTP_OK = 200;
const HTTP_BAD_REQUEST = 400;

it('Returns the url', () => {
  const url = wykop.generateConnectURL();
  return fetch(url).then((res) => {
    expect(res.status).toBeGreaterThanOrEqual(HTTP_OK);
    expect(res.status).toBeLessThan(HTTP_BAD_REQUEST);
    return res.text();
  }).then((res) => {
    expect(res.includes('Łączysz konto')).toBe(true);
  });
});
