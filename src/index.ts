import md5 from 'js-md5';
import qs from 'querystring';
import fetch, { Headers } from 'node-fetch';
import atob from 'atob';
import btoa from 'btoa';
import isMaciej from './utils/isMaciej';
import {
  IConnectData, IConstructorParams, ILoginData, IRequestParams, IUserData,
} from './models';

class Wykop {
  config: {
    appkey: string;
    secret: string;
    host: string;
    userAgent: string;
  };

  user: IUserData;

  constructor(config: IConstructorParams) {
    this.config = {
      appkey: 'aNd401dAPp',
      secret: '',
      host: 'a2.wykop.pl',
      userAgent: 'wykop-v2-js/dev',
      ...config,
    };

    this.user = {
      isLoggedIn: false,
      login: null,
      password: null,
      accountkey: null,
      userkey: null,
    };
  }

  private requestConfig(type: string, { api, named, post }: IRequestParams) {
    let url: string;
    url = `https://${this.config.host}/${type}/`;
    if (api) {
      url += `${api.join('/')}/`;
    }
    if (named) {
      url += `${Object.keys(named)
        .map((key) => `${key}/${named[key]}`)
        .join('/')}/`;
    }
    url += `appkey/${this.config.appkey}/`;
    if (this.user.isLoggedIn && this.user.userkey) {
      url += `userkey/${this.user.userkey}`;
    }

    const headers = new Headers({
      'User-Agent': this.config.userAgent,
    });
    if (post) {
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }
    // if signing header required
    if (!isMaciej(this.config.appkey)) {
      let signedContent = this.config.secret + url;
      if (post) {
        signedContent += Object.values(post).join(',');
      }
      headers.append('apisign', md5(signedContent));
    }

    let body: string | undefined;
    if (post) {
      body = qs.stringify(post);
    }

    const method = post ? 'POST' : 'GET';

    return {
      a: url,
      b: {
        body,
        method,
        headers,
      },
    };
  }

  public async request(type: string, { api, named, post }: IRequestParams) {
    const config = this.requestConfig(type, { api, named, post });
    return new Promise((resolve, reject) => fetch(config.a, config.b)
      .then((res) => res.json())
      .catch((err: TypeError) => { throw err; })
      .then((res: any) => {
        if (!res.error) {
          resolve(res);
        } else {
          // TODO: relogin if userkey expired and retry
          reject(res.error);
        }
      }));
  }

  public async login(data: ILoginData | string) {
    if (typeof data === 'object') {
      if ((data.login && data.accountkey)
        || (data.login && data.password && isMaciej(this.config.appkey))) {
        return this.loginCallback(data);
      }
    }

    // handling data from Login/Connect
    if (typeof data === 'string') {
      const d = JSON.parse(atob(data)) as IConnectData;
      if (d.appkey !== this.config.appkey) {
        throw new Error('Connect data for wrong appkey');
      }
      if (d.sign !== md5(this.config.secret + d.appkey + d.login + d.token)) {
        throw new Error('Manipulated connect data');
      }

      return this.loginCallback({
        login: d.login,
        accountkey: d.token,
      });
    }

    if (typeof data === 'undefined') {
      if (!this.user.login) throw new Error('Expected user.login');

      if (this.user.accountkey) {
        return this.loginCallback({
          login: this.user.login,
          accountkey: this.user.accountkey,
        });
      }

      if (!this.user.accountkey && this.user.password) {
        if (isMaciej(this.config.appkey)) {
          return this.loginCallback({
            login: this.user.login,
            password: this.user.password,
          });
        }
      }
    }

    throw new Error('No parseable login data');
  }

  private async loginCallback(data: ILoginData) {
    return this.request('login/index', {
      post: data,
    }).then((res: any) => {
      this.user.accountkey = data.accountkey;
      this.user.isLoggedIn = true;
      this.user.login = res.data.profile.login;
      this.user.password = data.password;
      this.user.userkey = res.data.userkey;

      return res.data.profile;
    });
  }

  public generateConnectURL(redirect?: string) {
    let url = `https://${this.config.host}/login/connect/appkey/${this.config.appkey}/`;
    if (redirect) {
      url += `redirect/${encodeURIComponent(btoa(redirect))}/`;
      url += `secure/${md5(this.config.secret + redirect)}/`;
    }
    return url;
  }
}

module.exports = Wykop;
export default Wykop;
export { isMaciej };
