# wykop-v2-js

## Wykop API v2 SDK in TypeScript

This code simplifies using the Wykop API in Node.js applications.

To use it in your project:

```bash
$ npm install wykop-v2
# or
$ yarn add wykop-v2
```
TypeScript declaration files are included.

Wykop API keys can be generated here: https://www.wykop.pl/dla-programistow/nowa-aplikacja/

Wykop API documentation can be found here: https://www.wykop.pl/dla-programistow/apiv2docs/.
Also the [old API documentation](https://www.wykop.pl/dla-programistow/dokumentacja/) is useful if anything is unclear in the new API docs.

```js
import Wykop from 'wykop-v2';
// or in CommonJS:
const Wykop = require('wykop-v2');

const wykop = new Wykop({
  // These are default values.
  appkey: 'aNd401dAPp',
  secret: '',
  host: 'a2.wykop.pl',
  userAgent: `wykop-v2-js/dev`,
});

wykop.login({
  login: 'm__b',
  accountkey: 'accountkey', // Get it with Wykop Connect
  // You can use the password instead of accountkey, if on a Maciej key.
  // Note that this way you must also do 2FA if it's enabled on the account
  // password: 'P455phR453'
}).then(() => wykop.request('entries/voteup', {
  api: ['6820916'],
}));
```
